import Adw from "gi://Adw";
import GObject from "gi://GObject";

import { ToggleAppearancePreferencePage } from "./pages/appearance.js";
import { ToggleDesktopPreferencePage } from "./pages/desktop.js";
import { ToggleDevicesPreferencePage } from "./pages/devices.js";
import { ToggleStartupAppsPreferencePage } from "./pages/startup-apps.js";

GObject.type_ensure(ToggleAppearancePreferencePage.$gtype);
GObject.type_ensure(ToggleDesktopPreferencePage.$gtype);
GObject.type_ensure(ToggleDevicesPreferencePage.$gtype);
GObject.type_ensure(ToggleStartupAppsPreferencePage.$gtype);

export class TogglePreferencesWindow extends Adw.Bin {
  static {
    GObject.registerClass(
      {
        GTypeName: "TogglePreferencesWindow",
        Template: "resource:///app/drey/Toggle/ui/preferences.ui",
      },
      this
    );
  }

  constructor(params?: Partial<Adw.Bin.ConstructorProperties>) {
    super(params);
  }
}
