import Adw from "gi://Adw";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

export class ToggleUnsupportedEnvironmentWindow extends Adw.Bin {
  private _picture!: Gtk.Picture;

  static {
    GObject.registerClass(
      {
        GTypeName: "ToggleUnsupportedEnvironmentWindow",
        Template: "resource:///app/drey/Toggle/ui/unsupported-environment.ui",
        InternalChildren: ["picture"],
        Signals: {
          warning_dismissed: {},
        },
      },
      this
    );
  }

  constructor(params?: Partial<Adw.Bin.ConstructorProperties>) {
    super(params);

    this._picture.set_resource(
      "/app/drey/Toggle/illustrations/unsupported-environment.svg"
    );
  }

  private warning_dismissed_cb() {
    this.emit("warning_dismissed");
  }
}
