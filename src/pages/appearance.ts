import Adw from "gi://Adw";
import GObject from "gi://GObject";
import Pango from "gi://Pango";
import Gtk from "gi://Gtk?version=4.0";
import GLib from "gi://GLib";
import {
  bind_setting_map,
  get_cursor_resource_entries,
  get_resource_entries,
  bind_setting_array_index_to_any,
} from "./util.js";
import { FontSelectorComponent } from "../components/font-selector.js";

GObject.type_ensure(FontSelectorComponent.$gtype);

enum FontAntialiasing {
  None,
  Grayscale,
  Subpixel,
}

enum FontHinting {
  None,
  Slight,
  Medium,
  Full,
}

/**
 * The enums above are ordered and automatically indexed in the order they are
 * defined in their respective gsetting.
 *
 * The re-assignments below are to give the UI a different order than the
 * gsettings, as we would like the "fanciest" option to be on top.
 */
const FONT_ANTIALIASING_ORDER = [
  FontAntialiasing.Subpixel,
  FontAntialiasing.Grayscale,
  FontAntialiasing.None,
];

const FONT_HINTING_ORDER = [
  FontHinting.Full,
  FontHinting.Medium,
  FontHinting.Slight,
  FontHinting.None,
];

export class ToggleAppearancePreferencePage extends Adw.PreferencesPage {
  private _cursor_themes!: Adw.ComboRow;
  private _icon_themes!: Adw.ComboRow;
  private _interface_font!: FontSelectorComponent;
  private _documents_font!: FontSelectorComponent;
  private _monospace_font!: FontSelectorComponent;
  private _font_hinting!: Adw.ComboRow;
  private _font_antialiasing!: Adw.ComboRow;
  private _font_scaling_factor!: Adw.SpinRow;
  private _open_themes_directory!: Gtk.Button;
  private _open_fonts_directory!: Gtk.Button;

  static {
    GObject.registerClass(
      {
        GTypeName: "ToggleAppearancePreferencePage",
        Template: "resource:///app/drey/Toggle/ui/pages/appearance.ui",
        InternalChildren: [
          "cursor_themes",
          "icon_themes",
          "interface_font",
          "documents_font",
          "monospace_font",
          "font_hinting",
          "font_antialiasing",
          "font_scaling_factor",
          "open_themes_directory",
          "open_fonts_directory",
        ],
        Signals: {
          warning_dismissed: {},
        },
      },
      this
    );
  }

  constructor(params?: Partial<Adw.PreferencesPage.ConstructorProperties>) {
    super(params);

    this._open_themes_directory.connect("clicked", () => {
      GLib.spawn_command_line_async(
        `flatpak-spawn --host xdg-open ${GLib.getenv(
          "HOST_XDG_DATA_HOME"
        )}/icons`
      );
    });

    this._open_fonts_directory.connect("clicked", () => {
      GLib.spawn_command_line_async(
        `flatpak-spawn --host xdg-open ${GLib.getenv(
          "HOST_XDG_DATA_HOME"
        )}/fonts`
      );
    });

    const icon_resource_entries = get_resource_entries("icons");
    const cursor_resource_entries = get_cursor_resource_entries();

    this._icon_themes.set_model(Gtk.StringList.new(icon_resource_entries));

    this._cursor_themes.set_model(Gtk.StringList.new(cursor_resource_entries));

    bind_setting_array_index_to_any(
      "org.gnome.desktop.interface",
      "icon-theme",
      this._icon_themes,
      "selected",
      icon_resource_entries,
      "string"
    );

    bind_setting_array_index_to_any(
      "org.gnome.desktop.interface",
      "cursor-theme",
      this._cursor_themes,
      "selected",
      cursor_resource_entries,
      "string"
    );

    this.map_font_setting(
      "org.gnome.desktop.interface",
      "font-name",
      this._interface_font
    );

    this.map_font_setting(
      "org.gnome.desktop.interface",
      "document-font-name",
      this._documents_font
    );

    this.map_font_setting(
      "org.gnome.desktop.interface",
      "monospace-font-name",
      this._monospace_font
    );

    this.map_enum_setting(
      "org.gnome.desktop.interface",
      "font-hinting",
      this._font_hinting,
      FONT_HINTING_ORDER
    );

    this.map_enum_setting(
      "org.gnome.desktop.interface",
      "font-antialiasing",
      this._font_antialiasing,
      FONT_ANTIALIASING_ORDER
    );

    bind_setting_map(
      "org.gnome.desktop.interface",
      "text-scaling-factor",
      this._font_scaling_factor,
      "value",
      (settings, widget) => {
        widget.value = settings.get_double("text-scaling-factor");
      },
      (settings, widget) => {
        /**
         * This is a workaround for issue #19.
         * https://gitlab.com/OroWith2Os/toggle/-/issues/19
         *
         * An issue has been filed against GTK.
         * https://gitlab.gnome.org/GNOME/gtk/-/issues/6162
         */

        GLib.timeout_add(GLib.PRIORITY_DEFAULT, 1000, () => {
          settings.set_double("text-scaling-factor", widget.value);
          return GLib.SOURCE_REMOVE;
        });
      }
    );
  }

  private map_enum_setting(
    path: string,
    key: string,
    comboRow: Adw.ComboRow,
    enumOrder?: number[]
  ) {
    bind_setting_map(
      path,
      key,
      comboRow,
      "selected",
      (settings, widget) => {
        widget.selected = enumOrder
          ? enumOrder[settings.get_enum(key)]
          : settings.get_enum(key);
      },
      (settings, widget) => {
        settings.set_enum(
          key,
          enumOrder ? enumOrder[widget.selected] : widget.selected
        );
      }
    );
  }

  private map_font_setting(
    path: string,
    key: string,
    component: FontSelectorComponent
  ) {
    bind_setting_map(
      path,
      key,
      component,
      "font-desc",
      (settings, widget) => {
        widget.font_desc = Pango.FontDescription.from_string(
          settings.get_string(key)
        );
      },
      (settings, widget) => {
        settings.set_string(key, widget.font_desc.to_string());
      }
    );
  }
}
