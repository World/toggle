import Adw from "gi://Adw";
import GObject from "gi://GObject";
import Gtk from "gi://Gtk?version=4.0";

import {
  bind_setting,
  bind_setting_array_index_to_any,
} from "./util.js";

export class ToggleDevicesPreferencePage extends Adw.PreferencesPage {
  private _middle_click_paste!: Gtk.Switch;
  private _overview_shortcut!: Adw.ComboRow;

  static {
    GObject.registerClass(
      {
        GTypeName: "ToggleDevicesPreferencePage",
        Template: "resource:///app/drey/Toggle/ui/pages/devices.ui",
        InternalChildren: [
          "middle_click_paste",
          "overview_shortcut",
        ],
        Signals: {
          warning_dismissed: {},
        },
      },
      this
    );
  }

  constructor(params?: Partial<Adw.PreferencesPage.ConstructorProperties>) {
    super(params);

    bind_setting(
      "org.gnome.desktop.interface",
      "gtk-enable-primary-paste",
      this._middle_click_paste
    );

    bind_setting_array_index_to_any(
      "org.gnome.mutter",
      "overlay-key",
      this._overview_shortcut,
      "selected",
      ["Super_L", "Super_R"]
    );
  }
}
