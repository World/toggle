blueprints = custom_target('blueprints',
  input: files(
    'ui/components/font-selector.blp',
    'ui/pages/appearance.blp',
    'ui/pages/desktop.blp',
    'ui/pages/devices.blp',
    'ui/pages/startup-apps.blp',
    'ui/preferences.blp',
    'ui/warning.blp',
    'ui/unsupported-environment.blp',
    'ui/window.blp',
  ),
  output:  '.',
  command: [find_program('blueprint-compiler'), 'batch-compile', '@OUTPUT@', '@CURRENT_SOURCE_DIR@', '@INPUT@']
)

icon_dir = 'icons' / 'hicolor' / 'scalable' / 'apps'
install_data(
  'icons' / '@0@.svg'.format(application_id),
  install_dir:  datadir / icon_dir
)

icon_dir = 'icons' / 'hicolor' / 'symbolic' / 'apps'
install_data(
  'icons' / 'app.drey.Toggle-symbolic.svg',
  install_dir: datadir / icon_dir
)

desktop_conf = configuration_data()
desktop_conf.set('app-id', application_id)
desktop_file = i18n.merge_file(
  input: configure_file(
  	input: 'app.drey.Toggle.desktop.in.in',
  	output: '@BASENAME@',
  	configuration: desktop_conf
  ),
  output: '@0@.desktop'.format(application_id),
  install: true,
  install_dir: datadir / 'applications',
  po_dir: po_dir,
  type: 'desktop'
)
desktop_file_validate = find_program('desktop-file-validate', required: false)
if desktop_file_validate.found()
  test(
    'validate-desktop',
    desktop_file_validate,
    args: [
      desktop_file.full_path()
    ]
  )
endif

gsettings_conf = configuration_data()
gsettings_conf.set('app-id', application_id)
gsettings_conf.set('gettext-package', gettext_package)
configure_file(
  input: 'app.drey.Toggle.gschema.xml.in',
  output: '@0@.gschema.xml'.format(application_id),
  configuration: gsettings_conf,
  install: true,
  install_dir: schemadir
)

metainfo_conf = configuration_data()
metainfo_conf.set('app-id', application_id)
metainfo_conf.set('gettext-package', gettext_package)

metainfo_file = i18n.merge_file(
  input: configure_file(
  	input: 'app.drey.Toggle.metainfo.xml.in.in',
  	output: '@BASENAME@',
  	configuration: metainfo_conf
	),
  output: '@0@.metainfo.xml'.format(application_id),
  po_dir: po_dir,
  install: true,
  install_dir:  datadir / 'metainfo'
)

data_conf = configuration_data()
data_conf.set('app-id', application_id)

data_res = gnome.compile_resources(
  application_id + '.data',
  configure_file(
  	input: 'app.drey.Toggle.data.gresource.xml.in',
  	output: '@BASENAME@',
  	configuration: data_conf
	),
  gresource_bundle: true,
  install_dir: pkgdatadir,
  install: true,
  dependencies: [blueprints, metainfo_file]
)

appstreamcli = find_program('appstreamcli', required: false)
if appstreamcli.found()
  test(
    'validate-metainfo', appstreamcli,
    args: [
      'validate', '--no-net', '--explain', metainfo_file,
    ]
  )
endif
